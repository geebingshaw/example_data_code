 
 
mkdir([pathOnset '/cued_mental_simulation_replay'])

f_fwd = dir([pathOnset '/onsets-fwdMentalSimulation/sub*fwdMentalSimulation*all*raw*']); %fwd trials
f_bwd = dir([pathOnset '/onsets-bwdMentalSimulation/sub*bwdMentalSimulation*all*raw*']); %bwd trials

for ii=  1:length(f_bwd)    

    T51 = readtable(fullfile(f_fwd(ii).folder,f_fwd(ii).name));
    T52 = readtable(fullfile(f_bwd(ii).folder,f_bwd(ii).name));

    isfwdTrial = ones(size(T51,1),1);
    T51 = [T51(:,1:3) table(isfwdTrial) T51(:,4:end)];

    isfwdTrial = zeros(size(T52,1),1);
    T52 = [T52(:,1:3) table(isfwdTrial) T52(:,4:end)];

    T = sortrows([T51;T52],2);
    writetable(T,[pathOnset '/cued_mental_simulation_replay/' f_fwd(ii).name(1:7) 'replayonsets_raw.txt'])
end
 
rmdir([pathOnset '/onsets-fwdMentalSimulation/'],'s')
rmdir([pathOnset '/onsets-bwdMentalSimulation/'],'s')