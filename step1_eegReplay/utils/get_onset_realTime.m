function [timeinfo]=get_onset_realTime(cfg,Subjid)
% get real onset time for each data point (relative to start time of each RUN
times = cfg.datainfo.times;  % onset time of each trial

subeventTime = cfg.eventTime{cfg.SubjID==Subjid}{1};

% for cued mental simulation
if strcmpi(cfg.task,'fwdMentalSimulation')
    trial_index = cfg.datainfo.trial_index(cfg.datainfo.idx_51);
    trial_runNum = cfg.datainfo.trials_runNumber(cfg.datainfo.idx_51);
elseif strcmpi(cfg.task,'bwdMentalSimulation')
    trial_index = cfg.datainfo.trial_index(cfg.datainfo.idx_52);
    trial_runNum = cfg.datainfo.trials_runNumber(cfg.datainfo.idx_52);
else
    trial_index = 1;
    trial_runNum = 1;
end
% trial_index,trial_runNum, times, subeventTime
%%
% reset onset time for each RUN (32 trials per run)
subeventTime(33:64) =  subeventTime(33:64) - subeventTime(33);
subeventTime(65:end) =  subeventTime(65:end) - subeventTime(65);

points_TrialNum = repmat(trial_index(:),1,length(times))';
points_TrialNum = points_TrialNum(:);

points_runNum = repmat(trial_runNum(:),1,length(times))';
points_runNum = points_runNum(:);

trial_onsets = subeventTime(trial_index)*1000;
trial_onsets = round(trial_onsets);

points_time  = repmat(trial_onsets(:),1,length(times))+ ...
               repmat(times,length(trial_index),1);
points_time = points_time';
points_time = points_time(:);


timeinfo = table(points_time, points_TrialNum, points_runNum );

 