function [sfAll, sbAll, is]=TDLM_EEG_core( is,trianFile,replayFile,sub_order)
 
is.ENL1 = 0.001;
is.lgncy = 1:60;

maxLag = length(is.lgncy);  
nstates=is.Num_state;       % number of state/stimuli/pitures
point=is.tss;
%% train classifier
d=load([char(trianFile )]); %load function locailizer data

% Prepare Dataset(traindata, nulldata) for training classifer
labeldata = d.marker_type;
true_data = squeeze(d.stim_data(:,point,:));

data_train_raw = true_data';
[data_train,scaleFact] =scaleFunc(data_train_raw);

% train classifiers on each stimulus in the order of picA, picB, picC, and picD
for iC=1:is.Num_state
    l1p = is.ENL1; l2p = 0;   % alpha = L1/(2*L2+L1) ; lambda = 2*L2+L1
    [beta, fitInfo] = lassoglm(data_train, labeldata == iC, 'binomial', ...
        'Alpha', l1p / (2*l2p+l1p), 'Lambda', 2*l2p + l1p, 'Standardize', false);
    gnf{iC,1}.beta = beta; gnf{iC,1}.Intercept = fitInfo.Intercept;
end

clear tureData nullData labels  stim_data
%% applay classifiers on testing data (cued mental simulation, resting)
d=load ([char(replayFile )] ); %load rest data
is.datainfo = d.datainfo;

data = d.data';
data = data ./ scaleFact;
Rreds = nan(size(data,1),is.Num_state);
for iC=1:is.Num_state
    % use regression models to detect probability of stimulus reactivation
    Rreds(:,iC) = 1 ./ (1 + exp(-(data * gnf{iC}.beta + gnf{iC}.Intercept)));
end

% save prob in the order of piture
is.Prob_pic = Rreds;

%% Getting Sequenceiness
% set permutation matrix
[~, uniquePerms] = uperms(1: is.Num_state);
uniquePerms = sortrows(uniquePerms,[1 2 3 4]);
is.nShuf=size(uniquePerms,1); %

%  switch the order of picture reactivation probability to match the state order
Rreds2 = Rreds(:,sub_order);

X=Rreds2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nbins=maxLag+1;

warning off
dm=[toeplitz(X(:,1),[zeros(nbins,1)])];
dm=dm(:,2:end);

for kk=2:nstates
    temp=toeplitz(X(:,kk),[zeros(nbins,1)]);
    temp=temp(:,2:end);
    dm=[dm temp];       
end

warning on

Y=X;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
betas = nan(nstates*maxLag, nstates);
% Set bins=10 to control alpha oscillation, or set it to maxLag for no control.
bins=10;  % 10 sample, 100 ms

for ilag=1:bins 
    temp_zinds = (1:bins:nstates*maxLag) + ilag - 1;
    temp = pinv([dm(:,temp_zinds) ones(length(dm(:,temp_zinds)),1)])*Y;  
    betas(temp_zinds,:)=temp(1:end-1,:);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
betasnbins=reshape(betas,[maxLag nstates^2]);
sf_temp = nan(maxLag,is.nShuf);
sb_temp = nan(maxLag,is.nShuf);
for iShuf=1:is.nShuf

    rp = uniquePerms(iShuf, :); %  when iShuf=1, rp=[1 2 3 4], not shuffled

    T1 = is.T(rp,rp);   % forward transition        
    T2 = T1';           % backward transition

    bbb=pinv([T1(:) T2(:) squash(eye(nstates)) squash(ones(nstates))])*(betasnbins');
    sf_temp(:,iShuf)=bbb(1,:);
    sb_temp(:,iShuf)=bbb(2,:);
end

sfAll = nan(1,is.nShuf,length(is.lgncy)+1);
sbAll = nan(1,is.nShuf,length(is.lgncy)+1);

sfAll(1,:,2:end) = sf_temp';
sbAll(1,:,2:end) = sb_temp';

is.Prob_State = Rreds2;
