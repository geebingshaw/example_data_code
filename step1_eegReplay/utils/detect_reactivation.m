function detect_reactivation( is,trianFile,testFile)
 
is.ENL1 = 0.001;

point=is.tss;
%% train classifier
d=load([char(trianFile )]); %load function locailizer data

% Prepare Dataset(traindata, nulldata) for training classifer
labeldata = d.marker_type;
true_data = squeeze(d.stim_data(:,point,:));

data_train_raw = true_data';
[data_train,scaleFact] =scaleFunc(data_train_raw);

% train classifiers on each stimulus in the order of picA, picB, picC, and picD
for iC=1:is.Num_state
    l1p = is.ENL1; l2p = 0;   % alpha = L1/(2*L2+L1) ; lambda = 2*L2+L1
    [beta, fitInfo] = lassoglm(data_train, labeldata == iC, 'binomial', ...
        'Alpha', l1p / (2*l2p+l1p), 'Lambda', 2*l2p + l1p, 'Standardize', false);
    gnf{iC,1}.beta = beta; gnf{iC,1}.Intercept = fitInfo.Intercept;
end

clear tureData nullData labels  stim_data
%% applay classifiers on test data (cued mental simulation, resting)
d=load ([char(testFile )] ); %load rest data
is.datainfo = d.datainfo;

data = d.data';
data = data ./ scaleFact;
Rreds = nan(size(data,1),is.Num_state);
for iC=1:is.Num_state
    % use regression models to detect probability of stimulus reactivation
    Rreds(:,iC) = 1 ./ (1 + exp(-(data * gnf{iC}.beta + gnf{iC}.Intercept)));
end

% save prob in the order of piture
is.Prob_pic = Rreds;

%% get onset time for each data point

Subjid = regexp(testFile,'sub(.*?)_','tokens');
Subjid = str2num(Subjid{1}{1});
timepoint_info=get_onset_realTime(is,Subjid);
%% get picture/stimuli reactivation probability
prob = array2table(is.Prob_pic,'VariableNames',{'A' 'B' 'C' 'D'});

piture_prob = [timepoint_info prob];

onsetPath = fullfile(is.pathResults,[strrep(is.tag,' ','_') '_reactivation_onsets']);
if ~exist(onsetPath,'dir'),mkdir(onsetPath);end

sfilename = fullfile(onsetPath,sprintf('sub_%02d_%s_prob.txt',...
    Subjid,is.task));
writetable(piture_prob,sfilename ); 
%%




