function [SeqOnsets]= sequenceness_onsets(X,T,lags)
% input:
%       X   - reactivation probility, time by state matrix
%       T   - transform matrix,  state by state matrix
%       lag - lags to compute, 1D array
% output:
%       SeqOnsets - time by lag onsets

fprintf('Computing onsets\n')
warning off
%% Core
SeqOnsets=[];
for iind=1:length(lags)
    lag=lags(iind);

    % pairwise transitions
    ToR1=zeros(size(X,1),1);
    orig = X(1:(end-2*lag),:)*T;
    proj = X((1+lag):(end-lag),:); % move down
    ToR1(1:(end-2*lag),1) = nansum((orig) .* (proj),2);
    SeqOnsets(:,iind)  =  ToR1;%
end
 
warning on