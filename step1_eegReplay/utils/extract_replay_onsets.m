function extract_replay_onsets(cfg,Subjid)
fprintf('extract replay onsets for sub-%02d\n',Subjid)
onsetPath = fullfile(cfg.pathResults, ['onsets-' cfg.task]);
if ~exist(onsetPath,"dir")
    mkdir(onsetPath)
end

timepoint_info=get_onset_realTime(cfg,Subjid);
%% compute replay onset for forward replay
SeqOnsets = sequenceness_onsets(cfg.Prob_State,cfg.T,1:4);
prob_fwd_lag20 = SeqOnsets(:,2);    % 2 in sample, 20 ms in time
prob_fwd_lag30 = SeqOnsets(:,3);
prob_fwd_lag40 = SeqOnsets(:,4);

SeqOnsets1 = [timepoint_info  table(prob_fwd_lag20,prob_fwd_lag30,prob_fwd_lag40) ];
sfilename = fullfile(onsetPath,sprintf('sub_%02d_%s_fwd_onset_raw.txt',...
    Subjid,cfg.task));
writetable(SeqOnsets1,sfilename );

%% compute replay onset for backward replay
SeqOnsets = sequenceness_onsets(cfg.Prob_State,cfg.T',1:4);
prob_bwd_lag20 = SeqOnsets(:,2);
prob_bwd_lag30 = SeqOnsets(:,3);
prob_bwd_lag40 = SeqOnsets(:,4);

SeqOnsets2 = [timepoint_info  table(prob_bwd_lag20,prob_bwd_lag30,prob_bwd_lag40) ];
sfilename = fullfile(onsetPath,sprintf('sub_%02d_%s_bwd_onset_raw.txt',...
    Subjid,cfg.task));
writetable(SeqOnsets2,sfilename );

%% merge
SeqOnsets_all = [SeqOnsets1 table(prob_bwd_lag20,prob_bwd_lag30,prob_bwd_lag40) ];
 
sfilename = fullfile(onsetPath,sprintf('sub_%02d_%s_all_onset_raw.txt',...
    Subjid,cfg.task));
writetable(SeqOnsets_all,sfilename );

