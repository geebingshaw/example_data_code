function h=sequenceness_results_plot(sfAll, sbAll)

nlag  = size(sfAll,3)-1;  
 
time_bin = 10; % 100 sample rate
cTime = 0:time_bin:nlag*time_bin;

%% GLM (fwd mean)
sf= sfAll;
dtp = squeeze(sf(:,1,:));

hh1=plot(cTime,dtp', 'color',[245 25 28]/256,'LineWidth',1  ); hold on,
xlabel('lag (ms)'), ylabel('Sequenceness')


%% GLM (bwd  mean)
sb= sbAll;
dtp = squeeze(sb(:,1,:));
hh2=plot(cTime, dtp','color',[52 144 233]/256,'LineWidth',1 ); hold on,
 
%%
legend([hh1 hh2],{'forward','backward'},'Box','off')
box off
drawnow
h={hh1;hh2;};
