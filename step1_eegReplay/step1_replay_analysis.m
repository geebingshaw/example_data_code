% TDLM analysis for EEG data

clc
clear
%% set environment
pathroot = fileparts(which('step1_replay_analysis'));
pathUtils  = fullfile(pathroot,'utils');
pathData   = fullfile(fileparts(pathroot),'data','eegData');
pathOnset  = fullfile(fileparts(pathroot),'data','replay_onsets');
pathResults = fullfile(fileparts(pathroot),'results','eeg_replay');
if ~exist(pathResults,'dir'),mkdir(pathResults);end
if ~exist(pathOnset  ,'dir'),mkdir(pathOnset  );end

addpath(pathUtils)
%% set sample
% Specify the IDs of the participants included in the study sample.
InGroupSubj = 21; % demo

%% define parameters for replay analysis
% the data points show the highest decoding accuracy in the functional localizer.
% We selected EEG data at this particular time point to train a model 
% and used this model to detect the reactivation probability of states.
cfg.tss =  41;  % 41 in sample, 210 ms in milisecond

% transition matrix 
Tfwd = [0 1 0 0; 0 0 1 0; 0 0 0 1; 0 0 0 0];  % forward transition
cfg.T = Tfwd;

cfg.Num_state = 4;

% the order of stimulus pictures is shuffled across subjects, and this matrix will realign them.
load Subj_state_order.mat
Subj_state_order = Subj_state_order(ismember(Subj_state_order.SubjID,InGroupSubj),:);
state_order = Subj_state_order.state_order;

% this matrix provides the onset time for each event, which is used to extract 
% the onset time of replay/reactivation for fMRI analysis.
load eventTime.mat
eventTime = eventTime.eventTime(ismember(eventTime.SubjID,InGroupSubj));
cfg.eventTime = eventTime;
 
% sample rate of EEG signal
cfg.srate = 100;

% get data files of functional localizer
tmp = struct2table(dir(fullfile(pathData, 'sub*_vfl.mat')));
files_FL  = {strcat(tmp.folder,filesep,tmp.name)};
if ~iscell(files_FL),files_FL = {files_FL};end

% get subject id from EEG filenames 
SubjID    = regexp(tmp.name,'\d+','match');
SubjID    = str2double(cat(1,SubjID{:}));
assert(isequal(SubjID,InGroupSubj'))

cfg.SubjID  =  SubjID;
cfg.pathResults = pathOnset; % extract replay onsets here
%% do replay analysis for cued mental simulation
% fwdMentalSimulation: cued by the sequence 1->2->3->4
% bwdMentalSimulation: cued by the sequence 4->3->2->1
taskStages = {'fwdMentalSimulation' 'bwdMentalSimulation' };
taskDiscription = {'forward mental simulation' 'backward mental simulation'};

figure('Color','w','Position',[0.1 0.1 0.55  0.73]*1000)
tiledlayout(3,2)
% Loop through all task stages
for task = [1 2 ]
 
    cfg.task   = taskStages{task};
    cfg.tag    = taskDiscription{task};

    % files of current task stage
    files_Replay  = struct2table(dir(fullfile(pathData, ['sub*_' cfg.task '.mat'])));
    files_Replay  = strcat(files_Replay.folder,filesep,files_Replay.name);
    if ~iscell(files_Replay),files_Replay = {files_Replay};end 

    % loop through all subjects
    for iSub = 1: numel(files_Replay)
        
        trianFile  = files_FL{iSub};
        replayFile = files_Replay{iSub};
        sub_order  = state_order(iSub,:);
        
        % compute sequenceness
        disp(['compute sequenceness for sub' num2str(SubjID(iSub))])
        [sfAll(iSub,:,:), sbAll(iSub,:,:), CFG{iSub}] = TDLM_EEG_core(cfg, ...
            trianFile,replayFile,sub_order);
        
        % extract replay onsets
        ccfg = CFG{iSub};
        extract_replay_onsets(ccfg,SubjID(iSub))
    end

    % save results 
    sfilename = [pathResults filesep 'sequence_' ...
        'vfl_' cfg.task  '.mat'];
    save(sfilename, 'sfAll', 'sbAll','CFG')
 
    % visualization
    nexttile
    h=sequenceness_results_plot(sfAll, sbAll);
    set(gca,'YLim',[-0.06 0.06])
    title(taskDiscription{task})
      
end
 
sfile = fullfile(pathResults,sprintf('sequenceness-%s-sub%d','EEG-fMRI',InGroupSubj));
savefig(gcf,sfile)

%% merge replay onsets
disp('merge replay onsets')
 
cd(pathroot)
merge_onsets

disp done
%% detect reactivation
taskStages = { 'preRest' 'postRest'};
taskDiscription = {'pre rest' 'post rest'};
% Loop through all task stages
for task = [1 2 ]
 
    cfg.task   = taskStages{task};
    cfg.tag    = taskDiscription{task};

    % files of current task stage
    files_Detect  = struct2table(dir(fullfile(pathData, ['sub*_' cfg.task '.mat'])));
    files_Detect  = strcat(files_Detect.folder,filesep,files_Detect.name);
    if ~iscell(files_Detect),files_Detect = {files_Detect};end

    % loop through all subjects
    for iSub = 1: numel(files_Detect)        
        trianFile  = files_FL{iSub};
        detectFile = files_Detect{iSub};
        
        detect_reactivation(cfg,trianFile,detectFile)
    end
end
