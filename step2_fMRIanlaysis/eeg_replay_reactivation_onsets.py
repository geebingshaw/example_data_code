# coding: utf-8
"""
# # SCRIPT: IDENTIFY EEG-BASED REACTIVATION ONSETS IN REST
# # PROJECT: FMRIREPLAY
# # WRITTEN BY QI HUANG 2022
# # CONTACT: STATE KEY LABORATORY OF COGNITIVE NEUROSCIENCE AND LEARNING, BEIJING NORMAL UNIVERSITY
"""

# In[1]: import relevant packages
import os
import sys
import warnings
import numpy as np
import pandas as pd
import pingouin as pg
import scipy.stats as stats
from os.path import join as opj
from joblib import Parallel, delayed
from scipy.interpolate import interp1d
from pandas.core.common import SettingWithCopyWarning
sys.path.append(os.getcwd())
from replay_onsets_functions import (confound_tr_task, behavior_start, spm_hrf,
                                     confound_tr_rest, pre_rest_start, post_rest_start)
warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)

# In[2]: set paths and subjects
path_bids = opj(os.getcwd().split('example_data_code')[0], 'example_data_code')
path_raw_behavior = opj(path_bids, 'data', 'raw_behavior_data')
path_behavior = opj(path_bids, 'data', 'behavior')
path_fmriprep = opj(path_bids, 'data', 'fmriprep')
path_raw_onsets = opj(path_bids, 'data', 'replay_onsets')
path_raw_onset_pre = opj(path_raw_onsets, 'pre_rest_reactivation_onsets')
path_raw_onset_post = opj(path_raw_onsets, 'post_rest_reactivation_onsets')
path_onsets = opj(path_bids, 'results', 'replay_onsets')
# time of repetition
time_repetition = 1.3
# the behavior file templates
templates = dict(events=opj(path_behavior, 'sub-21', 'sub-21_task-rep_run-0{run}_events.csv'),)
# example subject
sub = '21'

# In[3]: convolve the EEG-based replay onsets with HRF in task
# get the absolute time start point
abs_start = behavior_start(path_raw_behavior, sub)
# get the confound file to calculate the TR scans
scan_tr = confound_tr_task(path_fmriprep, sub)
# load the onset data file
onset_path = opj(path_raw_onsets, 'cued_mental_simulation_replay', 'sub_%s_replayonsets_raw.txt' % sub)
onset_file = pd.read_csv(onset_path, sep=',')
# load the behavior data file
beh_file = [pd.read_csv(templates['events'].format(run=run_id), sep=',', index_col=0) for run_id in range(1, 4)]
beh_file_cat = pd.concat(beh_file)
# recalculate the trial numbers
beh_file_cat['trials'] += 32 * (beh_file_cat['run'] - 1) + 1
# get the accurate trial based on behavior file
correct_trial = beh_file_cat.loc[beh_file_cat['accuracy'] == 1, 'trials'].astype(int)
# get the valid trial based on EEG replay file
eeg_trial = np.unique(onset_file['points_TrialNum'])
# get the intersection of the two files (final trials) for further correlation analysis, not for GLM.
intersection_set = np.array(list(set(correct_trial).intersection(set(eeg_trial))))
# get the fwd replay strength as the final replay strength
onset_file.loc[:, 'probability'] = onset_file.loc[:, 'prob_fwd_lag30']
# set the relative replay event onset
onset_file.loc[:, 'onset'] = np.nan
onset_file['onset'] = onset_file.loc[:, ['points_time']] / 1000
onset_file['onset'] = onset_file['onset'].apply(
    lambda x: float(str(x).split('.')[0] + '.' + str(x).split('.')[1][:2]))
onset_file.reset_index(drop=True, inplace=True)
# devide replay event into different files by runs
class_run = onset_file['points_runNum'].unique()
class_run.sort()
onset_run_data = [onset_file[onset_file['points_runNum'] == i] for i in class_run]
# rearrange the replay onset event
onset_run_event = [onset_run_data[i].loc[:, ['onset', 'probability']] for i in range(len(onset_run_data))]

# construct the replay onset file
for i in range(len(onset_run_event)):
    # get the absolute onset of replay events in the cued mental simulation task
    onset_run_event[i]['onset'] = onset_run_event[i]['onset'] + abs_start[i]
    onset_run_event[i]['onset'] = onset_run_event[i]['onset'].apply(
        lambda x: float(str(x).split('.')[0] + '.' + str(x).split('.')[1][:2]))
    # get the maximum time point in a run
    max_time = int(scan_tr[i]) * time_repetition
    # create the time frame list
    scan_list = pd.DataFrame(np.linspace(0, max_time, int(max_time * 100) + 1), columns=['onset'])
    scan_list['onset'] = scan_list['onset'].apply(
        lambda x: float(str(x).split('.')[0] + '.' + str(x).split('.')[1][:2]))
    scan_list['probability'] = 0
    # concatenate scan list and onset_run_event: set all the time points without replay onset to 0
    temp = pd.concat((onset_run_event[i], scan_list), axis=0)
    tempa = temp.drop_duplicates(subset='onset', keep='first').sort_values(by='onset').reset_index(drop=True)
    # spm HRF kernel
    hkernel = spm_hrf(tr=time_repetition, oversampling=100)
    # convolve the HRF kernel and probability event
    tempa['conv_reg'] = np.array(np.convolve(tempa['probability'], hkernel)[:tempa['probability'].size])
    # interpolation
    f = interp1d(tempa['onset'], tempa['conv_reg'])
    # get the specific time point BOLD signal (downsampling)
    slice_time_ref = 0.5
    start_time = slice_time_ref * time_repetition
    end_time = (scan_tr[i] - 1 + slice_time_ref) * time_repetition
    frame_times = np.linspace(start_time, end_time, scan_tr[i])
    # resample to the TR level for further GLM analysis
    resample_onset = pd.DataFrame(f(frame_times).T, columns=['replay_onsets'])
    # another method to downsampling (average) for comparison
    resample_onset_2 = pd.DataFrame([tempa[(tempa['onset'] >= ts * time_repetition) &
                                           (tempa['onset'] < (ts + 1) * time_repetition)]['conv_reg'].mean()
                                     for ts in range(scan_tr[i])], columns=['replay_onsets_mean'])
    resample_onset_2.reset_index(inplace=True, drop=True)
    resample_onset_all = pd.concat((resample_onset, resample_onset_2), axis=1)
    # save the data
    write_path = opj(path_onsets, 'sub-%s' % sub)
    if not os.path.exists(write_path):
        os.makedirs(write_path)
    file_name = 'sub-%s_task_run-%02.0f_replay_onsets.csv' % (sub, (i + 1))
    # replay onsets
    resample_onset_all.to_csv(opj(write_path, file_name), sep=',', index=0)


# In[4]: resting state reactivation probability onsets
# get the absolute time start point for pre resting and post resting state
abs_start_pre = pre_rest_start(path_raw_behavior, sub)
abs_start_post = post_rest_start(path_raw_behavior, sub)
# get the confound file to calculate the TR scans
scan_tr_pre = confound_tr_rest(path_fmriprep, sub)[0]
scan_tr_post = confound_tr_rest(path_fmriprep, sub)[1]
# pack the start time point and scan numbers
abs_start = [abs_start_pre, abs_start_post]
scan_tr = [scan_tr_pre, scan_tr_post]
# load the onset data file
pre_react = pd.read_csv(opj(path_raw_onset_pre, 'sub_%s_preRest_prob.txt' % sub), sep=',')
post_react = pd.read_csv(opj(path_raw_onset_post, 'sub_%s_postRest_prob.txt' % sub), sep=',')
# summarize all the states' probability
pre_react['probability'] = np.sum((pre_react['A'], pre_react['B'], pre_react['C'], pre_react['D']), axis=0)
post_react['probability'] = np.sum((post_react['A'], post_react['B'], post_react['C'], post_react['D']), axis=0)
# average the probability within whole sessions for paired t-test (Fig. 4a)
pre_react_mean = np.mean(pre_react['probability'], axis=0) / 4
post_react_mean = np.mean(post_react['probability'], axis=0) / 4
# EEG-based reactivation probability onsets in rest
consname = {0: {'cons': 'pre'}, 1: {'cons': 'post'}, }
for i, file in enumerate([pre_react, post_react]):
    # set the relative replay event onset
    file['onset'] = file.loc[:, ['points_time']] / 1000 + abs_start[i]
    file['onset'] = file['onset'].apply(lambda x: float(str(x).split('.')[0] + '.' + str(x).split('.')[1][:2]))
    file = file.loc[:, ['onset', 'probability']]
    # get the maximum time point in a run
    max_time = int(scan_tr[i]) * time_repetition
    # create the time frame list
    scan_list = pd.DataFrame(np.linspace(0, max_time, int(max_time * 100) + 1), columns=['onset'])
    scan_list['onset'] = scan_list['onset'].apply(lambda x: float(str(x).split('.')[0] + '.' + str(x).split('.')[1][:2]))
    scan_list['probability'] = 0
    # concatenate scan list and onset_run_event
    temp = pd.concat((file, scan_list), axis=0)
    tempa = temp.drop_duplicates(subset='onset', keep='first').sort_values(by='onset').reset_index(drop=True)
    # spm HRF kernel
    hrfkernel = spm_hrf(tr=time_repetition, oversampling=100)
    # convolve the HRF kernel and reactivation probability event
    tempa['conv_reg'] = np.array(np.convolve(tempa['probability'], hrfkernel)[:tempa['probability'].size])
    # interpolation
    f = interp1d(tempa['onset'], tempa['conv_reg'])
    # get the specific time point BOLD signal (downsampling)
    slice_time_ref = 0.5
    start_time = slice_time_ref * time_repetition
    end_time = (scan_tr[i] - 1 + slice_time_ref) * time_repetition
    frame_times = np.linspace(start_time, end_time, scan_tr[i])
    # resample BOLD signal
    resample_onset = pd.DataFrame(f(frame_times).T, columns=['reactivation_onsets'])
    # save the data
    file_name = 'sub-%s_%s_rest_eeg_reactivation_onsets.csv' % (sub, consname[i]['cons'])
    write_path = opj(path_onsets, 'sub-%s' % sub)
    if not os.path.exists(write_path):
        os.makedirs(write_path)
    resample_onset.to_csv(opj(write_path, file_name), sep=',', index=0)
