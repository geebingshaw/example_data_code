# coding: utf-8
"""
# # SCRIPT: FIRST LEVEL GLM IN EEG-BASED TASK REPLAY
# # PROJECT: FMRIREPLAY
# # WRITTEN BY QI HUANG 2022
# # CONTACT: STATE KEY LABORATORY OF COGNITIVE NEUROSCIENCE AND LEARNING, BEIJING NORMAL UNIVERSITY
"""

# In[1]: import relevant packages
import os
import warnings
import numpy as np
import pandas as pd
from os.path import join as opj
from joblib import Parallel, delayed
from nilearn.maskers import NiftiMasker
from nilearn import plotting, image, masking
from nilearn.image import load_img, resample_to_img, threshold_img
from nilearn.plotting import plot_design_matrix
from nilearn.glm.first_level import make_first_level_design_matrix, check_design_matrix, FirstLevelModel
from nilearn.glm.second_level import SecondLevelModel
from nilearn.glm import threshold_stats_img, cluster_level_inference
# filter out warnings related to the numpy package:
warnings.filterwarnings("ignore", message="numpy.dtype size changed*")
warnings.filterwarnings("ignore", message="numpy.ufunc size changed*")

# In[2]: set paths and subjects
# the analysis name
analysis_cons = 'eeg_task'
analysis_cons_2 = 'eeg_rest'
# the analysis path
path_bids = opj(os.getcwd().split('example_data_code')[0], 'example_data_code')
path_fmriprep = opj(path_bids, 'data', 'fmriprep')
path_behavior = opj(path_bids, 'data', 'behavior')
path_onsets = opj(path_bids, 'results', 'replay_onsets')
path_glm_l1 = opj(path_bids, 'results', 'glm', '%s-level1' % analysis_cons)
path_glm_l1_rest = opj(path_bids, 'results', 'glm', '%s-level1' % analysis_cons_2)

for path in [path_glm_l1, path_glm_l1_rest]:
    if not os.path.exists(path):
        os.makedirs(path)
# In[3]: set the functions


def _orthogonalize(X):
    if X.size == X.shape[0]:
        return X
    from scipy.linalg import pinv
    for i in range(1, X.shape[1]):
        X[:, i] -= np.dot(np.dot(X[:, i], X[:, :i]), pinv(X[:, :i]))
    return X


# set all the nan as mean value in confound variables
def replace_nan(regressor_values):
    # calculate the mean value of the regressor:
    mean_value = regressor_values.mean(skipna=True)
    # replace all values containing nan with the mean value:
    regressor_values[regressor_values.isnull()] = mean_value
    # return list of the regressor values:
    return list(regressor_values)


# In[4]: set some parameters
# example subject
sub = '21'
# time of repetition, in seconds:
time_repetition = 1.3
# the number of run
run_list = [1, 2, 3]
# set path templates
templates = dict(
    confounds=opj(path_fmriprep, 'sub-21', 'func',
                  'sub-21_task-replay_run-{run}_desc-confounds_timeseries.tsv'),
    events=opj(path_behavior, 'sub-21',
               'sub-21_task-rep_run-{run}_events.csv'),
    onset_events=opj(path_onsets, 'sub-21',
                     'sub-21_task_run-{run}_replay_onsets.csv'),
    func=opj(path_fmriprep, 'sub-21', 'func',
             'sub-21_task-replay_run-{run}_space-MNI152NLin2009cAsym_desc-preproc_bold.nii.gz'),
)
# set path templates
templates_rest = dict(
    confounds=opj(path_fmriprep, 'sub-21', 'func',
                  'sub-21_task-rest_run-{run}_desc-confounds_timeseries.tsv'),
    onset_events=opj(path_onsets, 'sub-21',
                     'sub-21_{cons}_rest_eeg_reactivation_onsets.csv'),
    func=opj(path_fmriprep, 'sub-21', 'func',
             'sub-21_task-rest_run-{run}_space-MNI152NLin2009cAsym_desc-preproc_bold.nii.gz'), )
# 2mm brain template
mni_mask = opj(path_bids, 'data/tpl-MNI152NLin2009cAsym_res-02_desc-brain_mask.nii')
mni_template = image.load_img(mni_mask)

# In[5]: GLM analysis for parametric modulation (level 1)
# set the design matrix
func_runs = []
design_matrices = []
# design matrix
for i, run_id in enumerate(run_list):
    # load the data path
    confounds = templates['confounds'].format(run=run_id)
    events = templates['events'].format(run=str('%02.f' % run_id))
    onset_events = templates['onset_events'].format(run=str('%02.f' % run_id))
    func = templates['func'].format(run=run_id)
    # load the data
    confounds_file = pd.read_csv(confounds, sep='\t')
    events_file = pd.read_csv(events, sep=',', index_col=0)
    onset_file = pd.read_csv(onset_events, sep=',')
    func_run = load_img(func)
    # confound variables
    regressor_names = ['trans_x', 'trans_y', 'trans_z', 'rot_x', 'rot_y', 'rot_z', 'csf', 'white_matter']
    regressors = [replace_nan(confounds_file[conf]) for conf in regressor_names]
    # mental simulation and wrong trial regressor
    events_file['trials'] += 32 * (events_file['run'] - 1) + 1
    events_file['trial_type'] = events_file['accuracy'].apply(
        lambda x: 'mental_simulation' if x == 1 else 'wrong')
    events = events_file.loc[:, ['trial_type', 'onset', 'duration']]
    # parameters of design matrix
    n_scans = func_run.shape[-1]
    frame_times = np.arange(n_scans) * time_repetition
    motion = np.transpose(np.array(regressors))
    design_matrix = make_first_level_design_matrix(
        frame_times, events, drift_model=None, add_regs=motion,
        add_reg_names=regressor_names, hrf_model='spm', oversampling=100)
    # concatenate the HRF-Convolved probability regressor
    onset_file.set_index(frame_times, inplace=True, drop=True)
    design_matrix = pd.concat((onset_file['replay_onsets'], design_matrix), axis=1)
    design_matrix_orth = pd.DataFrame((_orthogonalize(np.array(design_matrix.iloc[:, [0, 1]]))),
                                      columns=['replay_onsets', 'mental_simulation'],
                                      index=np.arange(int(n_scans)) * time_repetition)
    design_matrix_1 = pd.concat(
        (design_matrix_orth, design_matrix.drop(['replay_onsets', 'mental_simulation'], axis=1)), axis=1)
    design_matrices.append(design_matrix_1)
    func_runs.append(func_run)
# fit first level glm
fmri_glm = FirstLevelModel(t_r=time_repetition, slice_time_ref=0.5, hrf_model='spm',
                           drift_model=None, high_pass=1 / 128,
                           mask_img=mni_mask,
                           smoothing_fwhm=6, verbose=0,
                           noise_model='ar1', minimize_memory=True)
fmri_glm = fmri_glm.fit(run_imgs=func_runs, design_matrices=design_matrices)
# construct the contrasts
contrasts = {'replay_con': [], 'ms_con': [], }
for dm in design_matrices:
    contrast_matrix = np.eye(dm.shape[1])
    basic_contrasts = dict([(column, contrast_matrix[i])
                            for i, column in enumerate(dm.columns)])
    replay_con = basic_contrasts['replay_onsets']
    ms_con = basic_contrasts['mental_simulation']
    for contrast_id in ['replay_con', 'ms_con']:
        contrasts[contrast_id].append(eval(contrast_id))
# compute contrast
for index, (contrast_id, contrast_val) in enumerate(contrasts.items()):
    # Estimate the contrasts. Note that the model implicitly computes a fixed effect across the three sessions
    stats_map = fmri_glm.compute_contrast(contrast_val, stat_type='t', output_type='all')
    c_map = stats_map['effect_size']
    t_map = stats_map['stat']
    c_image_path = opj(path_glm_l1, '%s_cmap.nii.gz' % contrast_id)
    t_image_path = opj(path_glm_l1, '%s_tmap.nii.gz' % contrast_id)
    c_map.to_filename(c_image_path)
    t_map.to_filename(t_image_path)
    if index == 0:
        replay_beta = c_map
    elif index == 1:
        ms_beta = c_map


# In[5]: GLM analysis for parametric modulation (level 1) rest reactivation probability
# the number of rest run
rest_list = [1, 2]
# the name of rest run
cons_list = ['pre', 'post']
# design matrix
for cons, run_id in zip(cons_list, rest_list):
    # load the data path
    confounds = templates_rest['confounds'].format(run=run_id)
    onset_events = templates_rest['onset_events'].format(cons=cons)
    func = templates_rest['func'].format(run=run_id)
    # load the data
    confounds_file = pd.read_csv(confounds, sep='\t')
    onset_file = pd.read_csv(onset_events, sep=',')
    func_run = load_img(func)
    n_scans = func_run.shape[-1]
    # desired confound variables
    regressor_names = ['trans_x', 'trans_y', 'trans_z', 'rot_x', 'rot_y', 'rot_z', 'csf', 'white_matter']
    # create a nested list with regressor values
    regressors = [replace_nan(confounds_file[conf]) for conf in regressor_names]
    # confound variables
    motion = np.transpose(np.array(regressors))
    # frame_times for scan
    frame_times = np.arange(n_scans) * time_repetition  # here are the corresponding frame times
    design_matrix = make_first_level_design_matrix(
        frame_times, events=None, drift_model=None,
        add_regs=motion, add_reg_names=regressor_names,
        hrf_model='spm', oversampling=100)
    # concatenate the HRF-Convolved probability regressor
    onset_file.set_index(frame_times, inplace=True, drop=True)
    design_matrix = pd.concat((onset_file, design_matrix), axis=1)
    # GLM analysis
    fmri_glm = FirstLevelModel(t_r=time_repetition, slice_time_ref=0.5, hrf_model='spm',
                               drift_model=None, high_pass=1 / 128,
                               mask_img=mni_mask, smoothing_fwhm=6, verbose=0,
                               noise_model='ar1', minimize_memory=True)
    fmri_glm = fmri_glm.fit(run_imgs=func_run, design_matrices=design_matrix)

    # Rest contrast
    contrasts = {'reactivation': [], }  # contrast name
    contrast_matrix = np.eye(design_matrix.shape[1])
    basic_contrasts = dict([(column, contrast_matrix[i])
                            for i, column in enumerate(design_matrix.columns)])
    reactivation = basic_contrasts['reactivation_onsets']  # it is the regressor name, to form the contrast name
    for contrast_id in ['reactivation']:  # contrast name
        contrasts[contrast_id].append(eval(contrast_id))
    for index, (contrast_id, contrast_val) in enumerate(contrasts.items()):
        # Estimate the contrasts
        stats_map = fmri_glm.compute_contrast(contrast_val, stat_type='t', output_type='all')
        c_map = stats_map['effect_size']
        t_map = stats_map['stat']
        c_image_path = opj(path_glm_l1_rest, '%s_%s_cmap.nii.gz' % (cons, contrast_id))
        t_image_path = opj(path_glm_l1_rest, '%s_%s_tmap.nii.gz' % (cons, contrast_id))
        c_map.to_filename(c_image_path)
        t_map.to_filename(t_image_path)